import lightning.pytorch as pl
from lightning.pytorch.loggers import TensorBoardLogger
from lightning.pytorch.callbacks import EarlyStopping, LearningRateMonitor, ModelCheckpoint

from pytorch_forecasting import QuantileLoss
import warnings

from dataset import LightningDataModule
from models.arcane import Arcane
import torch

if __name__ == "__main__":
    torch.cuda.empty_cache()

    pl.seed_everything(42, workers=True)
    warnings.filterwarnings("ignore")  # avoid printing out absolute paths

    stocks = [
        "AAPL",
        "MSFT",
        "GOOG",
        "AMZN",
        "NVDA",
        "META",
        "TSLA",
        "AVGO",
        "ORCL",
        "NFLX",
        "AMD",
        "CSCO",
        "INTC",
        "SAP",
        "QCOM",
        "IBM",
        "TXN",
        "HON",
    ]

    data_module = LightningDataModule(
        stocks=stocks,
        max_prediction_length=30,
        max_encoder_length=1825,
        batch_size=42
    )

    trainer = pl.Trainer(
        max_epochs=100,
        accelerator="cuda",
        devices=1,
        enable_model_summary=True,
        gradient_clip_val=0.1,
        callbacks=[
            EarlyStopping(
                monitor="val_loss",
                min_delta=1e-4,
                patience=15,
                verbose=True,
                mode="min"
            ),
            LearningRateMonitor(),
            ModelCheckpoint(
                save_top_k=1,
                mode='min',
                monitor="val_loss",
            ),
        ],
        logger=TensorBoardLogger("./"),
    )

    arcane = Arcane(
        learning_rate=0.05,
        hidden_size=128,
        lstm_layers=2,
        attention_head_size=4,
        dropout=0.1,
        hidden_continuous_size=37,
        output_size=7,  # 7 quantiles by default
        loss=QuantileLoss(),
        log_interval=10,  # uncomment for learning rate finder and otherwise, e.g. to 10 for logging every 10 batches
        reduce_on_plateau_patience=5,
    )

    # fit the models on the data - redefine the models with the correct learning rate if necessary
    trainer.fit(
        model=arcane,
        datamodule=data_module,
    )
