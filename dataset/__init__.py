from datetime import datetime
from typing import List

import pandas as pd
import pytorch_lightning as pl
import pytz
from pytorch_forecasting import TimeSeriesDataSet, GroupNormalizer
import yfinance as yf
from torch.utils.data import DataLoader


class LightningDataModule(pl.LightningDataModule):
    def __init__(self, stocks: List, max_prediction_length: int, max_encoder_length: int, batch_size: int, num_workers: int  = 0):
        super().__init__()
        self.stocks = stocks
        self.max_prediction_length = max_prediction_length
        self.max_encoder_length = max_encoder_length
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.training_cutoff = None
        self.data = None
        self.prepare_data()
        self.train_dataset = None
        self.val_dataset = None

    def prepare_data(self) -> None:
        # download dataset start the time_idx from 1850-1-1
        utc_dt = datetime(1850, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        loc_dt = utc_dt.astimezone(pytz.timezone('America/New_York'))
        loc_dt.strftime('%Y-%m-%d %H:%M:%S %Z%z')

        # combine all the tickers from yahoo finance into one large pandas dataframe
        data = pd.DataFrame()
        for stock in self.stocks:
            ticker = yf.Ticker(stock)
            df = ticker.history(period='max', interval='1d')
            df = df.reset_index()
            df["Stock"] = stock
            df["time_idx"] = (df["Date"] - loc_dt).dt.days
            df["Day"] = df["Date"].dt.dayofweek
            df["Month"] = df["Date"].dt.month
            df["Quarter"] = df["Date"].dt.quarter
            df["Week_of_year"] = df["Date"].dt.isocalendar().week
            df["Season"] = df["Date"].dt.month.map(
                {
                    1: '1',
                    2: '1',
                    3: '2',
                    4: '2',
                    5: '2',
                    6: '3',
                    7: '3',
                    8: '3',
                    9: '4',
                    10: '4',
                    11: '4',
                    12: '1',
                }
            )

            df['EMA7'] = df['Close'].ewm(span=7, adjust=False).mean()
            df['EMA25'] = df['Close'].ewm(span=25, adjust=False).mean()
            df['EMA99'] = df['Close'].ewm(span=99, adjust=False).mean()

            df['SMA7'] = df['Close'].rolling(7).mean()
            df['SMA25'] = df['Close'].rolling(25).mean()
            df['SMA99'] = df['Close'].rolling(99).mean()
            # df['EMA360'] = df['Close'].ewm(span=360, adjust=False).mean()

            # remove all NAN values created by ema and sma columns
            df.drop(df.index[:99], inplace=True)
            df.drop('Stock Splits', axis=1)

            data = pd.concat([data, df])

        data = data.reset_index(drop=True)
        self.data = data.sort_values(by=["time_idx"])

    def setup(self, stage: str) -> None:
        self.training_cutoff = self.data["time_idx"].max() - self.max_prediction_length

        self.train_dataset = TimeSeriesDataSet(
            self.data[lambda x: x.time_idx <= self.training_cutoff],
            time_idx="time_idx",
            target="Close",
            group_ids=["Stock"],
            min_encoder_length=self.max_encoder_length // 2,
            max_encoder_length=self.max_encoder_length,
            max_prediction_length=self.max_prediction_length,
            # static_categoricals=["Sector"],
            # time_varying_known_categoricals=["Day", "Month", "Quarter", "Week_of_year"],
            time_varying_known_reals=[
                "time_idx",
                "Dividends",
                "Day",
                "Month",
                "Quarter",
                "Week_of_year",
                "Season",
                "EMA7",
                "EMA25",
                "EMA99",
                "SMA7",
                "SMA25",
                "SMA99",
            ],
            time_varying_unknown_reals=[
                "Open",
                "High",
                "Low",
                "Close",
                "Volume"
            ],
            target_normalizer=GroupNormalizer(
                groups=["Stock"],
                transformation="softplus"
            ),
            # group of categorical variables can be treated as one variable
            # variable_groups={"special_days": special_days},
            # add_relative_time_idx=True,
            add_target_scales=True,
            add_encoder_length=True,
            allow_missing_timesteps=True
        )

        # create validation set (predict=True) which means to predict the last max_prediction_length points in time
        # for each series
        self.val_dataset = TimeSeriesDataSet.from_dataset(self.train_dataset, self.data, predict=True,
                                                          stop_randomization=True)

    def train_dataloader(self):
        dl = DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            # pin_memory=True,
            num_workers=self.num_workers
        )
        return dl

    def val_dataloader(self):
        dl = DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            # pin_memory=True,
            num_workers=self.num_workers
        )
        return dl

